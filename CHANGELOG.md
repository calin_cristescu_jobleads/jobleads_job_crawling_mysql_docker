# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Open]

### To Add

### To Change

## [Unreleased]

### Added

### Changed

## [0.0.1](https://bitbucket.org/jobleads/jobleads_job_crawling_mysql_docker/src/?at=0.0.1) - released at 2018-08-29

### Added

* initial release
