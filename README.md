# Jobleads Job Crawling MySQL Docker Image

This repository contains the MySQL docker image for the jobleads job crawling project.

The current change log can be found [here](CHANGELOG.md).

# MySQL

This image comes with pre installed management gui.
Each time you start the docker container, you have to enable the plugin by login and run:
```
rabbitmq-plugins enable rabbitmq_management
```
Use `http://your_servers_ip:15672` to open the gui. Use user name `guest` and password `guest` to login.
