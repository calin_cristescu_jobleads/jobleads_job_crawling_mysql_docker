#!/bin/bash
####
# 
####
# @author Calin Cristescu <calin.cristescu@jobleads.de>
# @since 2018-08-29
####

NETWORK_DOCKER_NAME='jobleads_job_crawling_network'
PATH_OF_THIS_SCRIPT=$(cd $(dirname "$0"); pwd)

MYSQL_DOCKER_HOSTNAME='jobleads.job.crawling.mysql.local'
MYSQL_IMAGE_NAME='mysql'
MYSQL_IMAGE_TAG='5.7-percona'

ROOT_PASSWORD='6pbfYGsH'

function container_is_not_available
{
    local CONTAINER_PATTERN="${1}"

    if (docker image ls | grep -q "${CONTAINER_PATTERN}")
    then
        return 1;
    fi
}

function container_is_not_running
{
    local CONTAINER_NAME="${1}"

    if (docker container ls | grep -q "${CONTAINER_NAME}")
    then
        return 1;
    fi
}

function container_is_stopped
{
    local CONTAINER_NAME="${1}"

    if ! (docker container ls -a | grep -q "${CONTAINER_NAME}")
    then
        return 1;
    fi
}

####
# <string: path to the docker source>
# <string: docker image name>
# <string: docker image tag>
####
# @since: 2018-08-29
# @author: Calin Cristescu <calin.cristescu@jobleads.de>
####
function container_create_from_image
{
    local PATH_TO_THE_DOCKER_SOURCE="${1}"
    local DOCKER_IMAGE_NAME="${2}"
    local DOCKER_IMAGE_TAG="${3}"

    local PATH_TO_THE_DOCKER_SOURCE_README="${PATH_TO_THE_DOCKER_SOURCE}/README.md"

    echo ":: We have to build the docker container first."

    if [[ -f "${PATH_TO_THE_DOCKER_SOURCE_README}" ]];
    then
        echo ":: There is a README.md file."
        echo ":: Outputting the content now."
        echo "   Begin of README.md"
        cat "${PATH_TO_THE_DOCKER_SOURCE_README}"
        echo "   End of README.md"
    fi

    echo ":: Hit <ENTER> to continue."
    read -p ""

    echo ":: Start building image."
    echo "docker build -t ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} ${PATH_TO_THE_DOCKER_SOURCE}"
    docker build -t ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} ${PATH_TO_THE_DOCKER_SOURCE}
    echo ":: Finished building image."
}

####
# <string: container name>
# <string: network name>
# <string: image name>
function container_run_in_the_background
{
    local CONTAINER_NAME="${1}"
    local NETWORK_NAME="${2}"
    local IMAGE_NAME="${3}"

    echo ":: Run the container ${CONTAINER_NAME}."

    docker container run -e MYSQL_ROOT_PASSWORD=${ROOT_PASSWORD} -d --hostname ${CONTAINER_NAME} --name ${CONTAINER_NAME} --network ${NETWORK_NAME} ${IMAGE_NAME}
}

function container_create_and_login
{
    #local CONTAINER_NAME="${1}"
    local NETWORK_NAME="${1}"
    local IMAGE_NAME="${2}"
    local SOURCE_PATH="${3}"
    local DESTINATION_PATH="${4}"

    echo ":: Creating, run and logging in the container ${IMAGE_NAME}."

    docker container run --mount type=bind,source="${SOURCE_PATH}",target="${DESTINATION_PATH}" -e MYSQL_ROOT_PASSWORD=${ROOT_PASSWORD} --network ${NETWORK_NAME} -it ${IMAGE_NAME} /bin/ash
}

function container_login
{
    local CONTAINER_NAME="${1}"

    local CONTAINER_ID=$(docker container ls | grep ${CONTAINER_NAME} | cut -f 1 -d " ")

    docker container exec -it "${CONTAINER_ID}" /bin/ash
}

function container_start
{
    local CONTAINER_NAME="${1}"

    local CONTAINER_ID=$(docker container ls -a | grep ${CONTAINER_NAME} | cut -f 1 -d " ")

    echo ":: Starting container ${CONTAINER_NAME}."
    docker container start "${CONTAINER_ID}"
}

function container_stop
{
    local CONTAINER_NAME="${1}"

    local CONTAINER_ID=$(docker container ls | grep ${CONTAINER_NAME} | cut -f 1 -d " ")

    if [[ "${CONTAINER_ID}" == "" ]]
    then
        echo ":: Can not stop container ${CONTAINER_NAME}."
        echo "   Container is not running."
    else
        echo ":: Stopping container ${CONTAINER_NAME} with id ${CONTAINER_ID}."
        docker container stop "${CONTAINER_ID}"
    fi
}

function network_is_not_available
{
    local NETWORK_NAME="${1}"

    if (docker network ls | grep -q ${NETWORK_NAME})
    then
        return 1;
    fi
}

#if ! (docker network ls | grep -q ${NETWORK_DOCKER_NAME})
if network_is_not_available "${NETWORK_DOCKER_NAME}"
then
    echo ":: Creating network."
    docker network create -d bridge ${NETWORK_DOCKER_NAME}
fi

if container_is_not_available "${MYSQL_IMAGE_NAME}\s\+${MYSQL_IMAGE_TAG}"
then
    container_create_from_image $(realpath ${PATH_OF_THIS_SCRIPT}/../docker) ${MYSQL_IMAGE_NAME} ${MYSQL_IMAGE_TAG}
fi

if container_is_not_running "${MYSQL_DOCKER_HOSTNAME}"
then
    if container_is_stopped "${MYSQL_DOCKER_HOSTNAME}"
    then
        container_start "${MYSQL_IMAGE_NAME}:${MYSQL_IMAGE_TAG}"
    else
        container_run_in_the_background "${MYSQL_DOCKER_HOSTNAME}" "${NETWORK_DOCKER_NAME}" "${MYSQL_IMAGE_NAME}:${MYSQL_IMAGE_TAG}"
    fi
else
    echo ":: What do you want to do?"
    echo "   [0] login to ${MYSQL_DOCKER_HOSTNAME}"
    echo "   [1] stop ${MYSQL_DOCKER_HOSTNAME}"
    echo ""

    while true;
    do
        read USER_INPUT
        case ${USER_INPUT} in
            0)  container_login "${MYSQL_DOCKER_HOSTNAME}"
                break;;
            1)  container_stop "${MYSQL_DOCKER_HOSTNAME}"
                break;;
        esac
    done
fi
